import glob
import gi
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf


class ImageHandler:
    def __init__(self):
        super().__init__()
        self.all_markings = None
        self.marking_active = False
        self.project_name = None
        self.scroll_window = None
        self.v_adjust = None
        self.h_adjust = None
        self.layout = None
        self.image_viewer = None
        self.next_button = None
        self.previous_button = None
        self.images = None
        self.current_image_idx = None
        self.zoom_spin_button = None
        self.zoom_factor = 1
        self.image_width = 0
        self.image_height = 0

    def draw_markings(self):
        raise NotImplementedError

    def run_with_progress(self, generator):
        raise NotImplementedError

    def change_status_text(self, text):
        raise NotImplementedError

    def set_gui(self, gui_builder):
        self.scroll_window = gui_builder.get_object('scroll_window')
        self.v_adjust = self.scroll_window.get_vadjustment()
        self.h_adjust = self.scroll_window.get_hadjustment()
        self.layout = gui_builder.get_object('layout')
        self.image_viewer = gui_builder.get_object('image')
        self.next_button = gui_builder.get_object('next')
        self.next_button.set_sensitive(False)
        self.previous_button = gui_builder.get_object('previous')
        self.previous_button.set_sensitive(False)
        self.zoom_spin_button = gui_builder.get_object('zoom_spin_button')
        self.zoom_spin_button.set_sensitive(False)

    def prepare_image_gui(self):
        yield True
        self.images = sorted(glob.glob(self.project_name + '/opensfm/undistorted/*.jpg'))
        yield True
        self.current_image_idx = 0
        yield True
        temp_pix_buf = GdkPixbuf.Pixbuf.new_from_file(self.images[self.current_image_idx])
        yield True
        self.image_width = temp_pix_buf.get_width()
        self.image_height = temp_pix_buf.get_height()
        yield from self.set_scaled_image()
        self.activate_image_buttons()
        yield True

    def activate_image_buttons(self):
        self.next_button.set_sensitive(True)
        self.previous_button.set_sensitive(True)
        self.zoom_spin_button.set_sensitive(True)

    def zoom_on_image(self, zoom_value):
        self.zoom_factor = zoom_value/100
        self.run_with_progress(self.set_scaled_image)

    def set_scaled_image(self):
        yield True
        self.zoom_spin_button.set_sensitive(False)
        yield True
        width = int(self.image_width * self.zoom_factor)
        height = int(self.image_height * self.zoom_factor)
        yield True
        pix_buf = GdkPixbuf.Pixbuf.new_from_file_at_size(self.images[self.current_image_idx], width, height)
        yield True
        self.image_viewer.set_from_pixbuf(pix_buf)
        yield True
        self.layout.set_size(width, height)
        yield True
        self.zoom_spin_button.set_sensitive(True)
        yield True
        status_text = 'Current image: ' + self.images[self.current_image_idx].split('/')[-1]
        self.change_status_text(status_text)
        yield True

    def on_next_image_button(self, _=None):
        self.current_image_idx += 1
        self.run_with_progress(self.set_scaled_image)
        if self.current_image_idx == len(self.images) - 1:
            self.next_button.set_sensitive(False)
        if self.current_image_idx > 0:
            self.previous_button.set_sensitive(True)

    def on_previous_image_button(self, _=None):
        self.current_image_idx -= 1
        self.run_with_progress(self.set_scaled_image)
        if self.current_image_idx == 0:
            self.previous_button.set_sensitive(False)
        if self.current_image_idx < len(self.images) - 1:
            self.next_button.set_sensitive(True)

    def scroll(self, pressed, current):
        scroll_x = self.h_adjust.get_value()
        scroll_y = self.v_adjust.get_value()
        change_x = pressed[0] - current[0]
        change_y = pressed[1] - current[1]
        new_scroll_x = scroll_x + change_x
        self.h_adjust.set_value(new_scroll_x)
        new_scroll_y = scroll_y + change_y
        self.v_adjust.set_value(new_scroll_y)

    def jump_to_primary(self, mark):
        primary_mark = self.all_markings.get_primary(mark.primary)
        image = primary_mark.image
        for i, image_file in enumerate(self.images):
            if image_file.split('/')[-1] == image:
                self.current_image_idx = i
                break
        self.run_with_progress(self.set_scaled_image)

