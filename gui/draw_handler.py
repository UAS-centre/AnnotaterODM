import os
import cairo
import math
import numpy as np
import gi
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk, GdkPixbuf, GLib


class DrawHandler:
    def __init__(self):
        super().__init__()
        self.all_markings = None
        self.images = None
        self.current_image_idx = None
        self.marking_active = False
        self.draw_image = None
        self.draw_buf = None
        self.layout_height = 0
        self.layout_width = 0
        self.zoom_factor = 1
        self.draw_color = Gdk.RGBA(1, 0, 0, 1)
        self.draw_size = 10
        self.do_run_idle_tasks = True
        self.reproduction = None
        self.draw_center = None
        self.draw_point1 = None
        self.draw_point2 = None
        GLib.idle_add(self.do_draw_markings_when_idle().__next__)

    def do_draw_markings_when_idle(self):
        while self.do_run_idle_tasks:
            self.draw_markings()
            yield True
        yield False

    def stop_idle_tasks(self):
        self.do_run_idle_tasks = False

    def set_reproduction(self, reproduction):
        self.reproduction = reproduction

    def add_marking(self, point1, point2, point3):
        if self.marking_active:
            self.update_markings(point1, point2, point3)
            self.draw_markings()

    def scale_coordinates(self, coordinates, down=True):
        out_coordinates = []
        if down:
            for c in coordinates:
                out_coordinates.append(c / self.zoom_factor)
        else:
            for c in coordinates:
                out_coordinates.append(c * self.zoom_factor)
        return out_coordinates

    def update_markings(self, point1, point2, point3):
        points = (point1[0], point1[1], point2[0], point2[1], point3[0], point3[1])
        coordinates = self.scale_coordinates(points)
        ellipse_center = ((coordinates[0] + coordinates[2]) / 2, (coordinates[1] + coordinates[3]) / 2)
        ellipse_rot = np.arctan2(point1[1] - point2[1], point1[0] - point2[0])
        A = np.linalg.norm(np.array(ellipse_center) - np.array(coordinates[2:4]))
        B = np.linalg.norm(np.array(ellipse_center) - np.array(coordinates[4:]))
        ellipse_scale = (A, B)
        ellipse = [ellipse_center, ellipse_rot, ellipse_scale]
        image = self.images[self.current_image_idx].split(os.sep)[-1]
        self.all_markings.add(image, ellipse, self.reproduction)

    def on_layout_size_allocate(self, layout, _):
        width, height = layout.get_size()
        if width != self.layout_width or height != self.layout_height:
            self.scale_draw_image(width, height)
            self.layout_height = height
            self.layout_width = width

    def scale_draw_image(self, width, height):
        buf_new = self.draw_buf.scale_simple(width, height, GdkPixbuf.InterpType.BILINEAR)
        self.draw_image.set_from_pixbuf(buf_new)
        self.draw_buf = buf_new

    @staticmethod
    def pre_draw(draw_buf):
        width = draw_buf.get_width()
        height = draw_buf.get_height()
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
        cr = cairo.Context(surface)
        Gdk.cairo_set_source_pixbuf(cr, draw_buf, 0, 0)
        cr.paint()
        return cr, width, height

    def post_draw(self, cr, width, height):
        surface = cr.get_target()
        draw_buf = Gdk.pixbuf_get_from_surface(surface, 0, 0, width, height)
        self.draw_image.set_from_pixbuf(draw_buf)
        return draw_buf

    def draw_mark(self, cr, ellipse):
        if len(ellipse) == 2:
            cr.arc(ellipse[0], ellipse[1], self.draw_size, 0, 2*math.pi)
            cr.fill_preserve()
            cr.stroke()
        else:
            cr.set_line_width(3)
            cr.arc(ellipse[0][0], ellipse[0][1], self.draw_size, 0, 2*math.pi)
            cr.fill_preserve()
            cr.stroke()
            cr.save()
            cr.translate(ellipse[0][0], ellipse[0][1]);
            cr.rotate(ellipse[1])
            cr.scale(ellipse[2][0], ellipse[2][1])
            cr.arc(0, 0, 1, 0, 2*math.pi)
            cr.restore()
            cr.stroke()
            cr.save()

    def draw_ellipse_points(self, cr):
        if self.draw_center is not None:
            center = self.scale_coordinates(self.draw_center, False)
            cr.move_to(center[0], center[1])
            cr.arc(center[0], center[1], self.draw_size, 0, 2 * math.pi)
        if self.draw_point1 is not None:
            point1 = self.scale_coordinates(self.draw_point1, False)
            cr.move_to(point1[0], point1[1])
            cr.arc(point1[0], point1[1], self.draw_size / 2, 0, 2 * math.pi)
        if self.draw_point2 is not None:
            point2 = self.scale_coordinates(self.draw_point2, False)
            cr.move_to(point2[0], point2[1])
            cr.arc(point2[0], point2[1], self.draw_size / 2, 0, 2 * math.pi)
        cr.fill_preserve()
        cr.stroke()

    def set_point(self, point, point_str):
        if point is not None:
            if point_str == 'center':
                self.draw_center = self.scale_coordinates(point)
            elif point_str == 'point1':
                self.draw_point1 = self.scale_coordinates(point)
            elif point_str == 'point2':
                self.draw_point2 = self.scale_coordinates(point)
        else:
            self.draw_center = None
            self.draw_point1 = None
            self.draw_point2 = None

    def set_color(self, cr, marking):
        color = self.draw_color.copy()
        color.alpha = 0.3 + 0.7*marking.reproduction/100
        cr.set_source_rgba(*color)

    def scale_ellipse(self, ellipse):
        if len(ellipse) == 2:
            new_ellipse = self.scale_coordinates(ellipse, False)
        else:
            ellipse_center = self.scale_coordinates(ellipse[0], False)
            ellipse_rot = ellipse[1]
            ellipse_scale = (ellipse[2][0]*self.zoom_factor, ellipse[2][1]*self.zoom_factor)
            new_ellipse = [ellipse_center, ellipse_rot, ellipse_scale]
        return new_ellipse

    def draw_markings(self):
        if self.marking_active:
            draw_buf = self.draw_buf
            cr, width, height = self.pre_draw(draw_buf)
            image = self.images[self.current_image_idx].split(os.sep)[-1]
            for marking in self.all_markings.get_from_image(image):
                self.set_color(cr, marking)
                self.draw_mark(cr, self.scale_ellipse(marking.ellipse))
            self.draw_ellipse_points(cr)
            draw_buf = self.post_draw(cr, width, height)
            return draw_buf
