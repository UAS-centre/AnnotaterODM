import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class WarningDialog(Gtk.Dialog):
    def __init__(self, parent):
        header = 'You may loose data!'
        response = (Gtk.STOCK_NO, Gtk.ResponseType.NO, Gtk.STOCK_YES, Gtk.ResponseType.YES)
        Gtk.Dialog.__init__(self, title=header, transient_for=parent)
        self.add_buttons(*response)
        self.set_default_size(300, 50)
        label = Gtk.Label(label='If you clear all annotations data may be lost!')
        label2 = Gtk.Label(label='Continue?')
        box = self.get_content_area()
        box.add(label)
        box.add(label2)
        self.show_all()
