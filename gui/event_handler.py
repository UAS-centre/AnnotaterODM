from gui.draw_handler import DrawHandler
from gui.image_handler import ImageHandler
from gui.mouse_events import MouseEvents
from gui.warning_dialog import WarningDialog
from gui.status_bar import StatusBar
from gui.markings import Markings
import csv
import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib


class EventHandler(DrawHandler, ImageHandler, MouseEvents):
    def __init__(self, gui_builder, app_class):
        super().__init__()
        super().set_gui(gui_builder)
        self.status_bar = StatusBar(gui_builder)
        self.main_window = gui_builder.get_object('main_window')
        self.draw_image = gui_builder.get_object('draw_image')
        self.reproduction_toolbar = gui_builder.get_object('color_reproduction_toolbar')
        self.draw_buf = self.draw_image.get_pixbuf()
        self.app_class = app_class
        self.marking_active = False
        self.zoom_factor = 1
        self.all_markings = None
        self.project_name = None
        self.images = None
        self.current_image_idx = None
        self.save_annotations_file = 'annotations.csv'
        self.save_pref_file = 'pref.txt'
        self.setup_reproduction_combobox()

    def change_status_text(self, text):
        self.status_bar.change_text(text)

    def run_with_progress(self, gen_func):
        self.status_bar.start()
        generator = self.make_generator(gen_func)
        GLib.idle_add(generator.__next__, priority=GLib.PRIORITY_DEFAULT_IDLE)

    def make_generator(self, gen_func):
        yield from gen_func()
        self.status_bar.stop()
        yield False

    def scale_coordinates(self, coordinates, down=True):
        return super().scale_coordinates(coordinates, down)

    def add_marking(self, point1, point2, point3):
        super().add_marking(point1, point2, point3)

    def set_point(self, point, point_str):
        super().set_point(point, point_str)

    def scroll(self, pressed, realised):
        super().scroll(pressed, realised)

    def draw_markings(self):
        super().draw_markings()

    def jump_to_primary(self, mark):
        super().jump_to_primary(mark)

    def stop_idle_tasks(self):
        super().stop_idle_tasks()

    def on_open(self, *_):
        text = 'Choose a ODM project'
        action = Gtk.FileChooserAction.SELECT_FOLDER
        response = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        dialog = Gtk.FileChooserDialog(title=text, transient_for=self.main_window, action=action)
        dialog.add_buttons(*response)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.project_name = dialog.get_filename()
            dialog.destroy()
            self.run_with_progress(self.open_project)
        else:
            dialog.destroy()

    def open_project(self):
        yield True
        try:
            yield from self.prepare_image_gui()
            self.activate_menus()
            yield True
        except IndexError:
            print('Prepare gui failed')
        try:
            self.all_markings = Markings(self.project_name, self.images)
            yield True
            self.load_markings()
            yield True
        except IndexError:
            print('Loading markings failed')
        try:
            self.load_preferences()
            yield True
            self.marking_active = True
            yield True
        except IndexError:
            print('Loading preferences failed')
        yield True

    def activate_menus(self):
        self.app_class.next_image_action.set_enabled(True)
        self.app_class.previous_image_action.set_enabled(True)
        self.app_class.save_action.set_enabled(True)
        self.app_class.export_action.set_enabled(True)
        self.app_class.zoom_in_action.set_enabled(True)
        self.app_class.zoom_out_action.set_enabled(True)
        self.app_class.zoom_normal.set_enabled(True)
        self.app_class.clear_markings_action.set_enabled(True)

    def on_save(self, *_):
        if self.project_name:
            save_annotations = os.path.join(self.project_name, self.save_annotations_file)
            save_pref = os.path.join(self.project_name, self.save_pref_file)
            self.save_markings(save_annotations)
            self.save_preferences(save_pref)

    def on_export(self):
        text = 'Save Data as'
        action = Gtk.FileChooserAction.SAVE
        file_button = Gtk.STOCK_SAVE_AS
        response = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, file_button, Gtk.ResponseType.OK)
        dialog = Gtk.FileChooserDialog(title=text, transient_for=self.main_window, action=action)
        dialog.add_buttons(*response)
        dialog.set_do_overwrite_confirmation(True)
        dialog.set_current_name('untitled.csv')
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            save_file = dialog.get_filename()
            self.save_markings(save_file)
        dialog.destroy()

    def save_markings(self, save_file):
        with open(save_file, 'w') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(('image', 'ellipse draw_ellipse_points x', 'ellipse draw_ellipse_points y', 'ellipse rotation', 'ellipse a', 'ellipse b', 'size (m2)', 'reproduction', 'lat', 'lon'))
            for primary_id in self.all_markings.primary_markings_dict:
                mark = self.all_markings.id_dict[primary_id]
                row = (mark.image, mark.ellipse[0][0], mark.ellipse[0][1], mark.ellipse[1], mark.ellipse[2][0], mark.ellipse[2][1], mark.size, mark.reproduction, mark.lat_lon[0], mark.lat_lon[1])
                writer.writerow(row)

    def save_preferences(self, pref_file):
        with open(pref_file, 'w') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(('color', 'size'))
            writer.writerow((self.draw_color.to_string(), self.draw_size))

    def load_markings(self):
        file_name = os.path.join(self.project_name, self.save_annotations_file)
        csv_rows = self.csv_reader(file_name)
        if next(csv_rows):
            for row in csv_rows:
                if not row:
                    continue
                image = row[0]
                ellipse_center = (float(row[1]), float(row[2]))
                ellipse_rot = float(row[3])
                ellipse_scale = (float(row[4]), float(row[5]))
                ellipse = [ellipse_center, ellipse_rot, ellipse_scale]
                reproduction = int(row[7])
                self.all_markings.add(image, ellipse, reproduction)

    def load_preferences(self):
        file_name = os.path.join(self.project_name, self.save_pref_file)
        csv_reader = self.csv_reader(file_name)
        if next(csv_reader):
            for row in csv_reader:
                if not row:
                    continue
                self.draw_color.parse(row[0])
                self.draw_size = int(row[1])

    @staticmethod
    def csv_reader(file_name):
        if os.path.isfile(file_name):
            with open(file_name, 'r') as file:
                reader = csv.reader(file)
                for row in reader:
                    yield row
        else:
            yield False

    def clear_markings(self):
        dialog = WarningDialog(self.main_window)
        response = dialog.run()
        if response == Gtk.ResponseType.YES:
            self.all_markings = Markings(self.project_name, self.images)
            file = os.path.join(self.project_name, self.save_annotations_file)
            os.remove(file)
            self.draw_markings()
        dialog.destroy()

    def on_zoom_spinner(self, spin_button):
        zoom_value = spin_button.get_value_as_int()
        self.zoom_on_image(zoom_value)

    def setup_reproduction_combobox(self):
        reproduction_store = Gtk.ListStore(int, str)
        for rs in range(0, 101, 5):
            reproduction_store.append([rs, str(rs) + '%'])
        self.reproduction_combobox = Gtk.ComboBox.new_with_model(reproduction_store)
        renderer_text = Gtk.CellRendererText()
        self.reproduction_combobox.pack_start(renderer_text, True)
        self.reproduction_combobox.add_attribute(renderer_text, "text", 1)
        self.reproduction_combobox.connect('changed', self.on_reproduction_change)
        self.reproduction_toolbar.add(self.reproduction_combobox)
        self.reproduction_combobox.set_active(0)

    def on_reproduction_change(self, combobox):
        tree_iter = combobox.get_active_iter()
        if tree_iter is not None:
            model = combobox.get_model()
            super().set_reproduction(model[tree_iter][0])

    def delete_window(self, *_):
        self.on_save()
        self.stop_idle_tasks()
        self.main_window.destroy()
