import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class PreferencesDialog(Gtk.Dialog):
    def __init__(self, parent, color, size):
        self.color = color
        header = 'Preferences'
        response = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)
        Gtk.Dialog.__init__(self, title=header, transient_for=parent)
        self.add_buttons(*response)
        self.set_default_size(150, 100)
        color_label = Gtk.Label(label='Color:')
        color_button = Gtk.ColorButton()
        color_button.set_rgba(self.color)
        color_button.set_use_alpha(True)
        color_button.connect('color-set', self.on_color_chosen)
        size_label = Gtk.Label(label='Size:')
        size_adjustment = Gtk.Adjustment(value=size, lower=3, upper=50, step_increment=1, page_increment=5, page_size=0)
        self.size_entry = Gtk.SpinButton()
        self.size_entry.set_adjustment(size_adjustment)
        self.size_entry.set_numeric(True)
        grid = Gtk.Grid()
        grid.attach(color_label, 0, 0, 1, 1)
        grid.attach(color_button, 1, 0, 1, 1)
        grid.attach(size_label, 0, 1, 1, 1)
        grid.attach(self.size_entry, 1, 1, 1, 1)
        grid.set_column_spacing(20)
        box = self.get_content_area()
        box.pack_start(grid, True, True, 0)

        self.show_all()

    def on_color_chosen(self, button):
        self.color = button.get_rgba()

    def get_pref(self):
        size = self.size_entry.get_value_as_int()
        return self.color, size
