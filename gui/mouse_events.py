import os
import numpy as np
import gi
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk


class MouseEvents:
    def __init__(self):
        super().__init__()
        self.pressed_coordinates = None
        self.realised_coordinates = None
        self.marking_active = False
        self.dragging_mouse = False
        self.do_scroll = False
        self.all_markings = None
        self.images = None
        self.current_image_idx = None
        self.mark_clicked = None
        self.zoom_factor = None
        self.draw_size = None
        self.point1 = None
        self.point2 = None
        self.point3 = None

    def add_marking(self, point1, point2, point3):
        raise NotImplementedError

    def set_point(self, point, point_str):
        raise NotImplementedError

    def scroll(self, pressed, realised):
        raise NotImplementedError

    def draw_markings(self):
        raise NotImplementedError

    def jump_to_primary(self, mark):
        raise NotImplementedError

    def scale_coordinates(self, coordinates, down=True):
        raise NotImplementedError

    def mouse_not_moved(self):
        if abs(self.pressed_coordinates[0] - self.realised_coordinates[0]) > 5:
            return False
        elif abs(self.pressed_coordinates[1] - self.realised_coordinates[1]) > 5:
            return False
        return True

    def on_mouse_press(self, _, event):
        if self.marking_active:
            self.pressed_coordinates = (event.x, event.y)
            self.mark_clicked = self.clicked_on_mark()
            if event.button == 2:
                self.do_scroll = True
            self.dragging_mouse = True

    def on_mouse_release(self, _, event):
        if self.marking_active:
            self.realised_coordinates = (event.x, event.y)
            if event.button == 1:
                self.on_left_release(event)
            elif event.button == 3:
                self.on_right_release()
            self.dragging_mouse = False
            self.do_scroll = False

    def on_right_release(self):
        if self.mark_clicked and self.mouse_not_moved():
            if self.mark_clicked.primary is None:
                self.all_markings.remove(self.mark_clicked)
                self.draw_markings()

    def on_left_release(self, event):
        if not self.mark_clicked:
            if self.point1 is None:
                self.point1 = (event.x, event.y)
                self.set_point(self.point1, 'point1')
            elif self.point2 is None:
                self.point2 = (event.x, event.y)
                self.set_point(self.point2, 'point2')
                center = ((self.point1[0]+self.point2[0])/2, (self.point1[1]+self.point2[1])/2)
                self.set_point(center, 'center')
            elif self.point3 is None:
                self.point3 = (event.x, event.y)
                self.add_marking(self.point1, self.point2, self.point3)
                self.point1 = None
                self.point2 = None
                self.point3 = None
                self.set_point(None, '')
        elif self.mark_clicked.primary is not None and event.state & Gdk.ModifierType.CONTROL_MASK:
            self.jump_to_primary(self.mark_clicked)

    def mouse_move(self, _, event):
        if self.marking_active:
            current_coordinates = (event.x, event.y)
            if self.dragging_mouse and self.do_scroll:
                self.scroll(self.pressed_coordinates, current_coordinates)

    @staticmethod
    def get_dist(point1, point2):
        p1 = np.array(point1)
        p2 = np.array(point2)
        dist = np.linalg.norm(p1-p2)
        return dist

    def clicked_on_mark(self):
        pressed_coordinates = self.scale_coordinates(self.pressed_coordinates)
        min_dist = self.draw_size / self.zoom_factor
        mark = None
        image = self.images[self.current_image_idx].split(os.sep)[-1]
        for marking in self.all_markings.get_from_image(image):
            dist = self.get_dist(pressed_coordinates, marking.ellipse[0])
            if dist < min_dist:
                mark = marking
                min_dist = dist
        return mark
