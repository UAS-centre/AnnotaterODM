import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class AboutDialog(Gtk.Dialog):
    def __init__(self, parent):
        header = 'About'
        response = (Gtk.STOCK_OK, Gtk.ResponseType.OK)
        Gtk.Dialog.__init__(self, title=header, transient_for=parent)
        self.add_buttons(*response)
        self.set_default_size(300, 100)
        label = Gtk.Label(label='Multi Image Annotator')
        label2 = Gtk.Label(label='Made by:')
        label3 = Gtk.Label(label='Henrik Dyrberg Egemose')
        label4 = Gtk.Label(label='SDU UAS Center')
        box = self.get_content_area()
        box.add(label)
        box.add(label2)
        box.add(label3)
        box.add(label4)
        self.show_all()
