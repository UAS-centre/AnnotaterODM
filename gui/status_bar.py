import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Gdk


class StatusBar:
    def __init__(self, gui_builder):
        self.main_window = gui_builder.get_object('main_window')
        self.list_store = Gtk.ListStore(str, int)
        self.idx = self.list_store.append(['Test', 0])
        self.timeout_id = None
        self.is_active = False
        tree_view = Gtk.TreeView(model=self.list_store)
        tree_view.set_headers_visible(False)
        tree_view.set_sensitive(False)
        progress = Gtk.CellRendererProgress()
        progress.set_fixed_size(300, 30)
        column = Gtk.TreeViewColumn('Progress', progress, text=0, pulse=1)
        tree_view.append_column(column)
        toolbar = gui_builder.get_object('toolbar')
        tool_item = Gtk.ToolItem()
        tool_item.add(tree_view)
        toolbar.insert(tool_item, -1)

    def change_text(self, new_text):
        self.list_store[self.idx][0] = new_text

    def start(self):
        self.is_active = True
        self.set_cursor(False)
        GLib.timeout_add(50, self.on_timeout)

    def stop(self):
        self.is_active = False
        self.set_cursor(True)

    def on_timeout(self):
        if self.is_active:
            self.list_store[self.idx][1] += 2
            return True
        else:
            self.list_store[self.idx][1] = 0
            return False

    def set_cursor(self, arrow=True):
        if arrow:
            cursor = Gdk.Cursor(Gdk.CursorType.ARROW)
        else:
            cursor = Gdk.Cursor(Gdk.CursorType.WATCH)
        window = self.main_window.get_window()
        window.set_cursor(cursor)


