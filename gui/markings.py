import sys
if sys.platform == 'win32':
    from .odmConverter import OdmConverter
else:
    from gui.odmConverter import OdmConverter
from collections import defaultdict
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib


class Markings:
    def __init__(self, project, images):
        self.odm = OdmConverter(project)
        self.images = images
        self.identity = 0
        self.primary_markings_dict = defaultdict(list)
        self.image_dict = defaultdict(list)
        self.id_dict = {}

    def new_point(self, image, ellipse, reproduction, primary=None):
        mark_id = self.identity
        mark = Mark(mark_id, image, ellipse, reproduction, primary)
        self.id_dict.update({mark_id: mark})
        self.image_dict[image].append(mark_id)
        if primary is not None:
            self.primary_markings_dict[primary].append(mark_id)
        self.identity += 1
        return mark_id

    def add(self, image, ellipse, reproduction):
        mark_id = self.new_point(image, ellipse, reproduction)
        self.add_others(mark_id, image, ellipse, reproduction)
        # add_generator = self.add_generator(mark_id, image, ellipse, reproduction)
        # GLib.idle_add(add_generator.__next__, priority=GLib.PRIORITY_DEFAULT_IDLE)

    def add_others(self, mark_id, image, ellipse, reproduction):
        self.odm.set_image(image)
        lat_lon, size = self.odm.image_ellipse2geodetic(ellipse)
        mark = self.get_primary(mark_id)
        mark.size = size
        mark.lat_lon = lat_lon
        for img, coord in self.odm.geodetic2images_generator(*lat_lon):
            if img != image:
                self.new_point(img, coord, reproduction, mark_id)

    def add_generator(self, mark_id, image, ellipse, reproduction):
        yield True
        self.odm.set_image(image)
        yield True
        lat_lon, size = self.odm.image_ellipse2geodetic(ellipse)
        mark = self.get_primary(mark_id)
        mark.size = size
        mark.lat_lon = lat_lon
        yield True
        for img, coord in self.odm.geodetic2images_generator(*lat_lon):
            yield True
            if img != image:
                self.new_point(img, coord, reproduction, mark_id)
        yield False

    def get_from_image(self, image):
        for mark_id in self.image_dict[image]:
            mark = self.id_dict[mark_id]
            yield mark

    def get_from_primary(self, primary_id):
        for mark_id in self.primary_markings_dict[primary_id]:
            mark = self.id_dict[mark_id]
            yield mark

    def get_primary(self, primary_id):
        primary_mark = self.id_dict[primary_id]
        return primary_mark

    def remove(self, mark):
        self.id_dict.pop(mark.id)
        self.image_dict[mark.image].remove(mark.id)
        for mark_id in self.primary_markings_dict[mark.id]:
            image = self.id_dict[mark_id].image
            self.image_dict[image].remove(mark_id)
            self.id_dict.pop(mark_id)
        self.primary_markings_dict.pop(mark.id)


class Mark:
    def __init__(self, identity, image, ellipse, reproduction, primary=None):
        self.id = identity
        self.image = image
        self.ellipse = ellipse
        self.reproduction = reproduction
        self.size = None
        self.lat_lon = None
        self.primary = primary
