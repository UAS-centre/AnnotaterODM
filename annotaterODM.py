from gui.about_dialog import AboutDialog
from gui.event_handler import EventHandler
from gui.preferences_dialog import PreferencesDialog
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio


class App(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='org.annotate.odm',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.window = None
        self.handler = None
        self.preferences_action = None
        self.open_action = None
        self.save_action = None
        self.export_action = None
        self.quit_action = None
        self.next_image_action = None
        self.previous_image_action = None
        self.about_action = None
        self.zoom_in_action = None
        self.zoom_out_action = None
        self.zoom_normal = None
        self.clear_markings_action = None

    def do_startup(self):
        Gtk.Application.do_startup(self)
        self.preferences_action = self.make_action('preferences', self.on_preferences)
        self.open_action = self.make_action('open', self.on_open)
        self.save_action = self.make_action('save', self.on_save, False)
        self.export_action = self.make_action('export', self.on_export, False)
        self.clear_markings_action = self.make_action('clear_markings', self.on_clear_markings, False)
        self.quit_action = self.make_action('quit', self.on_quit)
        self.next_image_action = self.make_action('next_image', self.on_next_image, False)
        self.previous_image_action = self.make_action('previous_image', self.on_previous_image, False)
        self.about_action = self.make_action('about', self.on_about)
        self.zoom_in_action = self.make_action('zoom_in', self.on_zoom_in, False)
        self.zoom_out_action = self.make_action('zoom_out', self.on_zoom_out, False)
        self.zoom_normal = self.make_action('zoom_normal', self.on_zoom_normal, False)

    def do_activate(self):
        menu_builder = Gtk.Builder()
        menu_builder.add_from_file('gui/menu.glade')
        menu_bar = menu_builder.get_object('menu_bar')
        self.set_menubar(menu_bar)
        win_builder = Gtk.Builder()
        win_builder.add_from_file('gui/GUI.glade')
        self.handler = EventHandler(win_builder, self)
        win_builder.connect_signals(self.handler)
        self.window = win_builder.get_object('main_window')
        self.window.set_title('Multi Image Annotator')
        self.window.set_application(self)
        self.window.show_all()

    def make_action(self, name, func, enabled=True):
        action = Gio.SimpleAction.new(name, None)
        action.connect('activate', func)
        self.add_action(action)
        action.set_enabled(enabled)
        return action

    def on_about(self, *_):
        about_dialog = AboutDialog(self.window)
        response = about_dialog.run()
        if response:
            about_dialog.destroy()

    def on_open(self, *_):
        self.handler.on_open()

    def on_next_image(self, *_):
        self.handler.on_next_image_button()

    def on_previous_image(self, *_):
        self.handler.on_previous_image_button()

    def on_save(self, *_):
        self.handler.on_save()

    def on_export(self, *_):
        self.handler.on_export()

    def on_preferences(self, *_):
        preferences_dialog = PreferencesDialog(self.window, self.handler.draw_color, self.handler.draw_size)
        response = preferences_dialog.run()
        if response:
            args = preferences_dialog.get_pref()
            self.handler.draw_color = args[0]
            self.handler.draw_size = args[1]
        preferences_dialog.destroy()
        self.handler.draw_markings()

    def on_zoom_in(self, *_):
        zoom_factor = self.handler.zoom_spin_button.get_value_as_int()
        zoom_factor += self.handler.zoom_spin_button.get_increments().page
        self.handler.zoom_spin_button.set_value(zoom_factor)

    def on_zoom_out(self, *_):
        zoom_factor = self.handler.zoom_spin_button.get_value_as_int()
        zoom_factor -= self.handler.zoom_spin_button.get_increments().page
        self.handler.zoom_spin_button.set_value(zoom_factor)

    def on_zoom_normal(self, *_):
        self.handler.zoom_spin_button.set_value(100)

    def on_clear_markings(self, *_):
        self.handler.clear_markings()

    def on_quit(self, *_):
        self.handler.on_save()
        self.quit()


if __name__ == '__main__':
    app = App()
    app.run()
