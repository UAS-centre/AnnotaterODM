# AnnotaterODM

AnnotaterODM is an annotation tool to label images with ellipses using [OpenDroneMap](https://www.opendronemap.org/) to automatically find object in other images and get the area of the ellipse.

## Getting Started Ubuntu

### Prerequisites

* python 3 and pip
* PyGObject

### Installation

* Open a terminal in the folder where AnnotaterODM shall be.
* Clone the repository:  `git clone git@gitlab.com:UAS-centre/AnnotaterODM.git`
* Install dependencies: `pip install -r requirements.txt`

## Usage

```
python3 annotaterODM.py
```

### GUI usage:

Open a OpenDroneMap results folder to get started. The folder should contain the following folders and auto generated files within them:

- odm_georeferencing
- odm_orthophoto
- opensfm
- opensfm/depthmaps
- opensfm/undistorted

To add an ellipse the first 2 clicks are the ends of the ellipse along the long axis. The third click determine the second axis. The program will add circles of the center of the ellipses in the other images that also sees the same point.

#### Saved markings file format:
header: 'image', 'ellipse draw_ellipse_points x', 'ellipse draw_ellipse_points y', 'ellipse rotation', 'ellipse a', 'ellipse b', 'size (m2)', 'reproduction', 'lat', 'lon'

## Author

Written by Henrik Dyrberg Egemose (hesc@mmmi.sdu.dk) as part of the InvaDrone project a research project by the University of Southern Denmark UAS Center (SDU UAS Center).

## License

This project is licensed under the 3-Clause BSD License - see the [LICENSE](LICENSE) file for details.
